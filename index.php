<!doctype html>
<html>
<head>
	<meta charset="utf-8">
	<title>REXX - Code Challenge</title>
	<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
</head>

<body>
	<div class="container" style="padding-top:200px;">
	<?php
		include("database.php");

		$employee_name = $event_name = '';

		If($_POST && isset($_POST['save'])){
			$arr = array();
			$sql = "SELECT p.id, emp.name as employee_name, e.name as event_name, e.date,p.fee FROM participant p 
							left join event_master e on e.id = p.event_id
							left join employee_master emp on emp.id = p.employee_id
							where 1 ";
			$employee_name = $_POST['employee_name'];

			if($employee_name!=""){
				$sql .= " and emp.name like ?";
				$arr[] = '%'.$employee_name.'%';

			}
			$event_name = $_POST['event_name'];
			if($event_name!=""){
				$sql .=  " and e.name like ? ";
				$arr[] = '%'.$event_name.'%'; 
			}

			$participant = $conn->prepare($sql);
			$participant->execute($arr);
			
		}else{
			$participant = $conn->prepare("SELECT p.id,emp.name as employee_name, e.name as event_name, e.date , p.fee FROM participant p 
						left join event_master e on e.id = p.event_id
						left join employee_master emp on emp.id = p.employee_id");
		}
		$participant->execute();
		$result = $participant->fetchAll();
	?>
	<form method="POST" action="">
		<table class="table table-bordered">
		<thead>
			<th>Filter</th>
		</thead>
		<tbody>
			<tr><td>
				<label>Employee Name</label>&nbsp;
				<input type="text" name="employee_name" value="<?php echo $employee_name; ?>" class="form-control1" />&nbsp;
				<label>Event Name</label>&nbsp;
				<input type="text" name="event_name" value="<?php echo $event_name; ?>" class="form-control1" />&nbsp;
				<button type="submit" name="save" class="btn btn-primary">Submit</button>
				<button type="submit" name="reset" class="btn btn-primary">Reset</button>
			</td></tr>
		</tbody>
		</table>	
	</form>
	<table class="table table-bordered">
		<thead>
		<tr>
		<th>Sr. No.</th>
		<th>Employee Name</th>
		<th>Event Name</th>
		<th>Event Date</th>
		</tr>
		</thead>
		<tbody>

		<?php
			$cnt = 1;
			$fee = 0;
			foreach($result as $data )
			{

			echo"<td>".$cnt."</td>";
			echo"<td>".$data['employee_name']."</td>";
			echo"<td>".$data['event_name']."</td>";
			echo"<td>".$data['date']."</td>"; 
			echo "</tr>";
			$fee = $fee+ $data['fee'];
			$cnt++;
			}

			echo "<tr><td></td><td></td><td></td><td></td></tr>";
			echo "<tr><td></td><td><strong>Total Price (Fees)</strong></td><td><strong>".$fee."</strong></td><td></td></tr>";

		?>
		</table>
		</tbody>
	</div>

</html>