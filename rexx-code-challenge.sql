-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Feb 17, 2021 at 10:07 AM
-- Server version: 10.4.10-MariaDB
-- PHP Version: 7.3.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `rexx-code-challenge`
--

-- --------------------------------------------------------

--
-- Table structure for table `employee_master`
--

DROP TABLE IF EXISTS `employee_master`;
CREATE TABLE IF NOT EXISTS `employee_master` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `status` tinyint(1) DEFAULT 1,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COMMENT='employee master';

--
-- Dumping data for table `employee_master`
--

INSERT INTO `employee_master` (`id`, `name`, `email`, `status`) VALUES
(1, 'Reto Fanzen', 'reto.fanzen@no-reply.rexx-systems.com', 1),
(2, 'Leandro BuÃŸmann', 'leandro.bussmann@no-reply.rexx-systems.com', 1),
(3, 'Hans SchÃ¤fer', 'hans.schaefer@no-reply.rexx-systems.com', 1),
(4, 'Mia Wyss', 'mia.wyss@no-reply.rexx-systems.com', 1);

-- --------------------------------------------------------

--
-- Table structure for table `event_master`
--

DROP TABLE IF EXISTS `event_master`;
CREATE TABLE IF NOT EXISTS `event_master` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `date` datetime NOT NULL,
  `status` tinyint(1) DEFAULT 1,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COMMENT='event master';

--
-- Dumping data for table `event_master`
--

INSERT INTO `event_master` (`id`, `name`, `date`, `status`) VALUES
(1, 'PHP 7 crash course', '2019-09-04 08:00:00', 1),
(2, 'International PHP Conference', '2019-10-21 10:00:00', 1),
(3, 'code.talks', '2019-10-24 08:00:00', 1);

-- --------------------------------------------------------

--
-- Table structure for table `participant`
--

DROP TABLE IF EXISTS `participant`;
CREATE TABLE IF NOT EXISTS `participant` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `event_id` int(11) NOT NULL,
  `employee_id` int(11) NOT NULL,
  `fee` float NOT NULL,
  `version` varchar(255) NOT NULL,
  `status` tinyint(1) DEFAULT 1,
  PRIMARY KEY (`id`),
  KEY `event_id` (`id`),
  KEY `employee_id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COMMENT='participant employee in event';

--
-- Dumping data for table `participant`
--

INSERT INTO `participant` (`id`, `event_id`, `employee_id`, `fee`, `version`, `status`) VALUES
(1, 1, 1, 0, '1.0.17+42', 1),
(2, 2, 1, 1485.99, '1.0.17+59', 1),
(3, 2, 2, 657.5, '1.0.15+83', 1),
(4, 1, 3, 0, '1.0.17+65', 1),
(5, 1, 4, 0, '1.0.17+65', 1),
(6, 2, 4, 657.5, '1.1.3', 1),
(7, 3, 1, 474.81, '1.0.17+59', 1),
(8, 3, 3, 534.31, '1.1.3', 1);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
