<?php

	//database connection
	include 'database.php';

	//include json 
	$json = 'DEV_Events_full.json';
	$content = json_decode(file_get_contents($json));
		
	foreach($content as $data){

		//Event Master Insert based on event_id
		$event = $conn->prepare("SELECT * FROM `event_master` WHERE id = ?");
		$event->execute(array($data->event_id));
		$eventRow = $event->fetch();
		if(empty($eventRow)){
			$eventQuery = $conn->prepare("INSERT INTO `event_master` (id,name, date) values (?, ?, ?)");
			$event_id = $data->event_id;
			$event_name = $data->event_name;
			$event_date = $data->event_date;
			$eventQuery->execute(array($event_id, $event_name, $event_date));	
			$lastEventId = $conn->lastInsertId();
		}else{
			$lastEventId = isset($eventRow['id']) ? $eventRow['id'] : 0;
		}

		
		//We have not employee id in provided json , So we will conside email as unique and check email already exist or not and import employee data in employee master
		$emp = $conn->prepare("SELECT * FROM `employee_master` WHERE email = ?");
		$emp->execute(array($data->employee_mail));
		$empRow = $emp->fetch();
		if(empty($empRow)){
			$empQuery = $conn->prepare("INSERT INTO `employee_master` (name, email) values (?, ?)");
			$employee_name = $data->employee_name;
			$employee_mail = $data->employee_mail;
			$empQuery->execute(array($employee_name, $employee_mail));	
			$lastEmpId = $conn->lastInsertId();
		}else{
			$lastEmpId = isset($empRow['id']) ? $empRow['id'] : 0;
		}
		
		
		//Participant employee/user participant in event, So, we will save employee_id & event_id in participant table 
		//fee is related to participant - so, it will in participant table
		//version - we whave no more details. So, right now set in participant table but we can also create version master if more detail available 
		$participant = $conn->prepare("SELECT * FROM `participant` WHERE id = ?");
		$participant->execute(array($data->participation_id));
		$participantRow = $participant->fetch();
		if(empty($participantRow)){
			$empQuery = $conn->prepare("INSERT INTO `participant` (event_id, employee_id, fee, version) values (?, ?, ?, ?)");
			$participation_fee = $data->participation_fee;
			$version = $data->version;
			$empQuery->execute(array($lastEventId, $lastEmpId, $participation_fee, $version));	
		}
		
	}